import sys
import re


class Student:

    def __init__(self, line):
        self.line = line.strip()
        l = self.line.split("\t")
        self.id = l[0]
        self.score = -1.0

    def __str__(self):
        if self.score == -1:
            score = 0
        else:
            score = self.score
        return "{0}\t{1}".format(self.id, score)


def save_data(fname, students):
    f = open(fname, 'w')
    for i in students:
        f.write(str(i))
        f.write("\n")
    f.close()


def input_until_matched(prompt, pattern):
    while True:
        s = input(prompt)
        if re.match(pattern, s):
            return s
        else:
            print("Invalid value, please re-enter")
    return None

def list_students(students):
    for s in students:
        print(str(s))
    print()


if __name__ == "__main__":
    if len(sys.argv) > 1:
        fname = sys.argv[1]
    else:
        print("Unknown data file.")
        quit()

    f = open(fname, 'r')
    students = []
    for line in f:
        s = Student(line)
        students.append(s)
    f.close()

    for i in students:
        print(i)

    while True:
        cmd = input('> ')
        if cmd == 'save':
            save_data(fname, students)
            break
        elif cmd == 'quit':
            break
        elif cmd == 'list':
            list_students(students)

        pat = cmd
        if re.match('^[0-9]+$', cmd) is None:
            print("input only either ID or commands (save, quit, list).")
            continue

        matched = []
        i = 1
        for s in students:
            if re.search(pat, s.id):
                matched.append(s)
                print("({0}) {1}".format(i, s))
                i += 1

        count = len(matched)
        if count == 0:
            print("ID not found")
        else:
            while count > 1:
                schoice = input_until_matched('choice> ', '^[0-9]+$')
                choice = int(schoice)
                if choice <= count:
                    student = matched[choice - 1]
                    break
                else:
                    print("unknown choice")
            if count == 1:
                student = matched[0]
            sscore = input_until_matched('score> ', '^[0-9]+(.[0-9]*)?$')
            score = float(sscore)
            if student.score != -1:
                print("replaced!!!")
            student.score = score
            print("updated {0} to {1}".format(student.id, student.score))
