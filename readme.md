This script provides a CLI for inputting scores by specifying student ID.

## Usage
    python scoreinput.py [data file]

Input a student ID before inputting the score, or one of the commands (i.e.
save, quit, list)

## Data File
Data file is a tab-separated text file containing a list of student
IDs, one student per line. For example,

    1111
    2222
    3333
    4444

Each line may include other values.

    1111    John
    2222    Peter
    3333    Susan
    4444    Jane
